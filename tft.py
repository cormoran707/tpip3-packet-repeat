#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# Copyright (c) 2016 Kenya Ukai
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
#

import os
import zipfile
import ftplib
import telnetlib
import sys
import argparse

BLUE = '\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
ENDC = '\033[0m'


class TpipTelnet:

    def __init__(self):
        self.term = '$'

    def open(self, ip):
        self.telnet = telnetlib.Telnet(ip, timeout=2)

    def login(self, debug_print=False):
        raise NotImplementedError()

    def write(self, command, debug_print=False):
        self.telnet.write(command + '\n')
        ret = self.telnet.read_until(self.term)
        if debug_print:
            print(ret.replace(self.term, '$')),

    def exit(self, debug_print=False):
        self.telnet.write('exit\n')
        ret = self.telnet.read_all()
        if debug_print:
            print(ret)

    def close(self):
        self.telnet.close()


class Tpip3Telnet(TpipTelnet):

    def __init__(self):
        TpipTelnet.__init__(self)
        self.term = '$'

    def login(self, debug_print=False):
        ret = ""
        ret += self.telnet.read_until('freescale login:')
        print(ret)
        self.telnet.write('root\r\n')
        ret += self.telnet.read_until('Password')
        print(ret)
        self.telnet.write('root\n')
        ret += self.telnet.read_until(self.term)
        print(ret)
        if debug_print:
            print(ret.replace(self.term, '$'))


class Tpip2Telnet(TpipTelnet):

    def __init__(self):
        TpipTelnet.__init__(self)
        self.term = '\h[\u]\$'

    def login(self, debug_print=False):
        ret = ""
        ret += self.telnet.read_until("login: ")
        self.telnet.write('root\n')
        ret += self.telnet.read_until(self.term)
        if debug_print:
            print(ret.replace(self.term, '$'))


def zip_write(src, dest):
    base_dir = os.path.dirname(os.path.abspath(src))
    src = os.path.relpath(src, base_dir)

    zf = zipfile.ZipFile(dest, 'w', zipfile.ZIP_DEFLATED)

    # ファイルの時
    if not os.path.isdir(src):
        zf.write(src)
        zf.close()
        return
    # ディレクトリの時
    for root, dirs, files in os.walk(src):
        for file in files:
            filename = os.path.join(root, file)
            zf.write(filename)
        for dir in dirs:
            dirname = os.path.join(root, dir)
            zf.write(dirname)
    zf.close()


def send_file(tpip_ip, src, dest, telnet):
    try:
        src_zip = os.getcwd() + os.path.sep + os.path.basename(src) + '.zip'
        zip_write(src, src_zip)
        ftp = ftplib.FTP(tpip_ip, 'root', 'root', timeout=2)

        with open(src_zip, 'rb') as f:
            ftp.storbinary('STOR ' + dest + '.zip', f)
            ftp.quit()
            print("SEND OK : %s " % os.path.relpath(src))

        print('- ' * 10)
        telnet.open(tpip_ip)
        telnet.login(True)
        telnet.write('cd %s' % os.path.dirname(dest), True)
        telnet.write('rm -rf %s' %
                     os.path.basename(dest), True)  # TODO 危険な気がする
        telnet.write('unzip %s.zip' % os.path.basename(dest), True)
        telnet.exit(True)
        telnet.close()
        print('- ' * 10)
        print("UNZIP OK")
    except ftplib.all_errors, e:
        print(RED + 'ERROR : %s while sending %s' % (e, src) + ENDC)


def kill(tpip_ip, dest, telnet):
    try:
        telnet.open(tpip_ip)
        telnet.login(True)
        telnet.write('kill -INT `pidof %s`' % os.path.basename(dest), True)
        telnet.exit(True)
        telnet.close()
        print("KILL OK.")
    except ftplib.all_errors, e:
        print(RED + 'ERROR : %s' % e + ENDC)


def execute(tpip_ip, dest, telnet):
    try:
        dir = os.path.dirname(dest)
        command = os.path.basename(dest)
        telnet.open(tpip_ip)
        telnet.login(True)
        telnet.write('kill -INT `pidof %s`' % command, True)
        telnet.write("cd %s" % dir, True)
        telnet.write("chmod +x %s" % command, True)
        telnet.write(
            'nohup ./%s > /dev/null >> /dev/null < /dev/null &' % command, True)
        telnet.exit(True)
        telnet.close()
        print("EXECUTE OK : %s" % command)
    except ftplib.all_errors, e:
        print(RED + 'ERROR : %s' % e + ENDC)


def main(argv):
    parser = argparse.ArgumentParser('TpipFileTransfer')

    parser.add_argument('files', nargs='*',
                        help='files or directories execute after transfer')
    parser.add_argument('-e', '--execute', nargs='*',
                        help='files execute after transfer')
    parser.add_argument('-k', '--kill', nargs='*',
                        help='kill programs by \"kill -INT\" command on tpip')
    parser.add_argument('-i', '--ip', default='192.168.0.200',
                        help='destination tpip ip (default 192.168.0.200)')
    parser.add_argument('-t', '--target', default='TPIP3',
                        help='TPIP2 or TPIP3 (default TPIP3)')

    args = parser.parse_args(argv)
    args.target = args.target.upper()
    current = os.getcwd()
    tpip_ip = args.ip
    telnet = Tpip3Telnet() if args.target == "TPIP3" else Tpip2Telnet()

    print(
        """
        TpipFileTransfer

        ip : %s
        target ; %s
        """ % (tpip_ip, args.target))

    for f in args.files:
        send_file(tpip_ip,
                  os.path.abspath(f),
                  '/tmp/' + os.path.basename(f),
                  telnet)

    if args.execute:
        for f in args.execute:
            dest = '/tmp/' + os.path.basename(f)
            if os.access(f, os.X_OK):
                send_file(tpip_ip, os.path.abspath(f), dest, telnet)
            execute(tpip_ip, dest, telnet)

    if args.kill:
        for program in args.kill:
            kill(tpip_ip, program, telnet)


if __name__ == '__main__':
    main(sys.argv[1:])
