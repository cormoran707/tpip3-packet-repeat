# TPIP3 Packet Repeat

TPIP3でWifi側と有線LAN側のネットワークを簡易的につなぐ

---

## stone : Simple Repeater

古め(2003年とか)のCコンパイラでもコンパイルできて，最低限の機能を持ってそうなプログラムだったので使用

Lnuxカーネルに欲しい機能はあるので，カーネル再コンパイル権があればそうするのが無難そう

- URL

        http://www.gcd.org/sengoku/stone/Welcome.ja.html

- ライセンス : GPL

- 使い方

~~~
./stoneexe 転送先アドレス:ポート 自分が公開するポート
~~~

        例

        ~~~
        # 192.168.0.22 に raspberrypi がいて，50001番ポートで何かサービスを動かしている
        # TPIP3の8080番ポートにアクセスするとraspberrypiの50001番ポートにつながるようにする
        # 転送通信（telnet）終了後も動かし続ける
        ./stoneexe 192.168.0.22:50001 8080 &
        ~~~

- stoneexeはTPIP3用にarm用gccでコンパイルしたバイナリ

---

## tft.py : TPIP File Transfer .py

ファイルやフォルダをリモートに送り込む補助スクリプト

TPIP Wikiに似たようなプログラムがあるが，ライセンス的にだめなので自作

telnetを自動化しているだけ

機能

- ファイル・フォルダの転送（zip圧縮 -> 転送 -> 解凍）
- 実行ファイルを転送して実行（同名のコマンドは事前にkill）
- 実行されているプログラムをkill

---

## 2016年度の例

RaspberryPi でmjpegstreamerを２つ，lighthttpd(?)を動かし，TPIPへのアクセスをそこへリバースプロキシする．

`trans.sh/bat` をダブルクリックで転送・実行をしてくれる．

`stoneexe`と`run.sh`を送り込み，`run.sh`を実行する．